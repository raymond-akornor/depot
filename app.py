from datetime import datetime
import os
import json
from datetime import datetime

from flask import Flask, request, redirect, render_template, url_for, flash, abort, session
from flask_sqlalchemy import SQLAlchemy as SQLA
from flask_login import UserMixin, login_required, LoginManager, login_user, logout_user, current_user
from werkzeug.security import generate_password_hash, check_password_hash

basedir = os.path.abspath(os.path.dirname(__file__))

#app initializations
app = Flask(__name__)
app.config.update([('SQLALCHEMY_DATABASE_URI', 'sqlite:///' + os.path.join(basedir, 'data.sqlite')), ('SQLALCHEMY_TRACK_MODIFICATIONS', True)])
app.config['SECRET_KEY'] = os.urandom(24)

#database & login manager inits
db = SQLA(app)
login_manager = LoginManager(app)
login_manager.session_protection = 'strong'
login_manager.login_view = 'login'

# routes and view functions
@app.route('/')
def index():
	return "Welcome to Depot Management System"

@app.route('/home')
@login_required
def home():
	products = Product.query.all()
	if not products:
		abort(404)
	return render_template('home.html', products=products)

@app.route('/login', methods=('GET', 'POST'))
def login():
	if request.method == "POST":
		email = request.form['email']
		password = request.form['password']

		customer = Customer.query.filter_by(email=email).first()
		if customer is not None and customer.verify_password(password):
			login_user(customer, True)
			flash("Logged in successfully")
			return redirect(url_for('home'))
		else:
			flash('wrong username or password. Try logging in again')
			return redirect(url_for('login'))
	return render_template("login.html")

@app.route('/register', methods=('GET', 'POST'))
def register():
	if request.method == "POST":
		customer = Customer(request.form['name'], request.form['email'], request.form['phone_number'], request.form['address'], request.form['password'])
		db.session.add(customer)
		db.session.commit()
		flash('Customer successfully created an account. Kindly login')
		return redirect(url_for('login'))
	return render_template('register.html')

@app.route('/logout')
@login_required
def logout():
	logout_user()
	flash("You've been logged out")
	return redirect(url_for('index'))

## products

@app.route('/products')
def products():
	products = Product.query.all()
	return render_template('products.html', products=products)

@app.route('/product/<int:id>')
def product(id):
	product = Product.query.get_or_404(id)
	return render_template('product.html', product=product)

@app.route('/addcart/<int:id>', methods=('GET', 'POST'))
def add_cart(id):
	product = Product.query.get_or_404(id)
	if request.method == "POST":
		qty = request.form['quantity']
		if 'cart' not in session:
			session['cart'] = []
		session['cart'].append((product.product_id, product.description, product.price, qty))
		flash('successfully added to cart')
		print session['cart']
		return redirect('/cart')
	return render_template('add_cart.html', product=product)

	
@app.route('/cart')
@login_required
def cart():
	if 'cart' not in session:
		flash('There is nothing in your cart. kindly add items')
		return render_template('cart.html', cart=None, total=0)
	else:
		products = session['cart']
		dict_of_products = {}
		total_price = 0
		for id, description, price, qty in products:
			#print qty
			total_price += price * int(qty)
			dict_of_products[id] = {"qty":qty, "name":description, "price":price}
		return render_template('cart.html', cart=dict_of_products, total_price=total_price)

# @app.route('/remove/<int:id>')
# def remove(id):
# 	for idx, product in enumerate(session['cart']):
# 		for product_id, description, price, qty in product:
# 			if product_id == id:
# 				products.pop(idx)
# 				return redirect('/cart')
# 			else:
# 				flash('product not in cart')
# 				return redirect('/cart')

@app.route('/checkout')
def checkout():
	if 'cart' not in session:
		flash('Kindly add items to the cart before checkout')
		return redirect('/home')
	cart = session['cart']
	for product in cart:
		product_id, _, _, qty = product
		date = datetime.now().date()
		order = Order(current_user.customer_id, product_id, now, qty)
		db.session.add(order)
		db.session.commit()
	flash('You have successfully checked out')
	return redirect('/home')


# analytics
@app.route('/statistics')
def statistics():
	return render_template('analytics.html')

@app.route('/statistics/show_stats')
def show_stats():
	pass




@login_manager.user_loader
def load_customer(customer_id):
 	return Customer.query.get(int(customer_id))

## models
class Customer(db.Model, UserMixin):
	""" The Customer Model """
	__tablename__ = "customers"
	customer_id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String, index=True)
	password_hash = db.Column(db.String(128))
	email = db.Column(db.String(128), unique=True, index=True)
	address = db.Column(db.String)
	phone_number = db.Column(db.Integer, unique=True)

	def __init__(self, name, email, phone_number, address, password):
		self.name = name
		self.email = email
		self.phone_number = phone_number
		self.address = address
		self.password = password

	@property
	def password(self):
		raise AttributeError('password not a readable attribute')

	@password.setter
	def password(self, password):
		self.password_hash = generate_password_hash(password)

	def verify_password(self, password):
		return check_password_hash(self.password_hash, password)

	def is_active(self):
		return True

	def is_authenticated(self):
		return True

	def is_anonymous(self):
		return False

	def get_id(self):
		return unicode(self.customer_id)

	def __repr__(self):
		return '<Customer {}>'.format(self.customer_id)

class Product(db.Model):
	""" The Product Model """
	__tablename__ = 'products'
	product_id = db.Column(db.Integer, primary_key=True)
	description = db.Column(db.String, index=True, unique=True)
	price = db.Column(db.Float) #for fixed point precision

	def __repr__(self):
		return '<Product {} {}>'.format(self.description, self.price)

class Order(db.Model):
	""" The Customer Order Model """
	__tablename__ = "orders"
	order_id = db.Column(db.Integer, primary_key=True)
	order_date = db.Column(db.DateTime, default=datetime.utcnow())
	customer_id = db.Column(db.ForeignKey('customers.customer_id'))
	product_id = db.Column(db.ForeignKey('products.product_id'))
	ship_date = db.Column(db.DateTime)
	order_qty = db.Column(db.Integer)
	shipped = db.Column(db.Boolean, default=False)

	def __init__(self,customer_id, product_id, ship_date, order_qty):
		self.customer_id = customer_id
		self.product_id = product_id
		self.ship_date = ship_date
		self.order_qty = order_qty


if __name__ == '__main__':
	app.run(debug=True)