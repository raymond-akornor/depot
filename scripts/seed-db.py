## this short scripts seeds the database with products

import sys, os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))

import csv
from app import db, Product

def main():
	db.drop_all()
	db.create_all()
	with open('../data/products.csv') as f:
		data = csv.reader(f)
		headings = next(data)

		for row in data:
			id, description, price = row
			product = Product()
			product.description = description
			product.price = price

			db.session.add(product)
			db.session.commit()
	print "successfully added to database"


if __name__ == '__main__':
	main()
