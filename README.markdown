## how to install
* clone this repo
* cd into directory
* run following commands
  * `virtualenv env`
  * `source env/bin/activate`
  * `pip install -r requirements.txt`
  * `python app.py`
* open browser and go to `http://127.0.0.1:5000/`

## Structure

```sh
├── static/         # static assests such as js and css
├── app.py/         # main app
├── scripts/        # set up scripts and batch jobs
├── templates/      # jinja templates
```
